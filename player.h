#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <vector>
#include <climits>
#include <unistd.h>

#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *doMinimax(int depth);

    vector<Move> findMoves();
    vector<Move> findMoves(Side side);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Side side;
    Board board;
};


#endif
