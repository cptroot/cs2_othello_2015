So I started by implementing minimax and a custom heuristic.

The logic behind the simple custom heuristic is that corners are more valuable, because they can't be controlled by others. Similarly, edges are more valuable than center squares, but less valuable than corners. 

The spaces directly next to corners are special. Often, control of them leads to the opponent gaining a corner. Similarly, the spaces next to edges provide a similar lead sometimes.

After some heuristic manipulation, I found a local maximum that would beat BetterPlayer. I saved that in case I couldn't improve the algorithm to a point where it would still beat BetterPlayer.

The first improvement made during week 2 was to add AB pruning. This decreased the time so that I could add one more depth to the minimax search.

After the AB pruning, I attempted to improve the heuristic by valuing mobility and stable spaces.

Both of these will hopefully improve my algorithms chances against other algorithms than BetterPlayer. In addition, I think that they decrease the sensitivity to heuristic modifications that I was struggling with, where small changes to the values of the heuristics would result in large changes in result.
