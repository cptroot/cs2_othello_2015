#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    this->side = side;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

int stable_values [WIDTH * HEIGHT];
int print = false;

int stable(Board* board, Side side) {
  int result = 0;
  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < HEIGHT; j++) {
      stable_values[i + j * WIDTH] = 0;
    }
  }
  //stable_values[0] = 0xFF;
  //stable_values[WIDTH - 1] = 0xFF;
  //stable_values[(HEIGHT - 1) * WIDTH] = 0xFF;
  //stable_values[WIDTH * HEIGHT - 1] = 0xFF;

  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < HEIGHT; j++) {
      if (i == 0 || j == 0) {
        stable_values[i + j * WIDTH] |= board->get(side, i, j) << 0;
      } else {
        stable_values[i + j * WIDTH] |= (board->get(side, i, j) << 0) & stable_values[(i - 1) + (j - 1) * WIDTH] & (1 << 0);
      }

      if (j == 0) {
        stable_values[i + j * WIDTH] |= board->get(side, i, j) << 1;
      } else {
        stable_values[i + j * WIDTH] |= (board->get(side, i, j) << 1) & stable_values[i + (j - 1) * WIDTH] & (1 << 1);
      }

      if (i == 0) {
        stable_values[i + j * WIDTH] |= board->get(side, i, j) << 7;
      } else {
        stable_values[i + j * WIDTH] |= (board->get(side, i, j) << 7) & stable_values[(i - 1) + j * WIDTH] & (1 << 7);
      }
    }
  }

  for (int i = WIDTH - 1; i >= 0; i--) {
    for (int j = HEIGHT - 1; j >= 0; j--) {
      if (i == WIDTH - 1 || j == HEIGHT - 1) {
        stable_values[i + j * WIDTH] |= board->get(side, i, j) << 4;
        stable_values[i + (HEIGHT - 1 - j) * WIDTH] |= board->get(side, i, (HEIGHT - 1 - j)) << 2;
        stable_values[(WIDTH - 1 - i) + j * WIDTH] |= board->get(side, (WIDTH - 1 - i), j) << 6;
      } else {
        stable_values[i + j * WIDTH] |= (board->get(side, i, j) << 4) & stable_values[(i + 1) + (j + 1) * WIDTH] & (1 << 4);
        stable_values[i + (HEIGHT - 1 - j) * WIDTH] |= (board->get(side, i, HEIGHT - 1 - j) << 2) & stable_values[(i + 1) + (HEIGHT - 1 - j - 1) * WIDTH] & (1 << 2);
        stable_values[(WIDTH - 1 - i) + j * WIDTH] |= (board->get(side, WIDTH - 1 - i, j) << 6) & stable_values[(WIDTH - 1 - i - 1) + (j + 1) * WIDTH] & (1 << 6);
      }

      if (j == HEIGHT - 1) {
        stable_values[i + j * WIDTH] |= board->get(side, i, j) << 5;
      } else {
        stable_values[i + j * WIDTH] |= (board->get(side, i, j) << 5) & stable_values[i + (j + 1) * WIDTH] & (1 << 5);
      }

      if (i == WIDTH - 1) {
        stable_values[i + j * WIDTH] |= board->get(side, i, j) << 3;
      } else {
        stable_values[i + j * WIDTH] |= (board->get(side, i, j) << 3) & stable_values[(i + 1) + j * WIDTH] & (1 << 3);
      }
    }
  }

  int value;
  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < HEIGHT; j++) {
      value = stable_values[i + j * WIDTH];
      if (print)
        fprintf(stderr, "%2x ", value);
      if (      ((value >> 0) & 1 || (value >> 4) & 1) &&
                ((value >> 1) & 1 || (value >> 5) & 1) &&
                ((value >> 2) & 1 || (value >> 6) & 1) &&
                ((value >> 3) & 1 || (value >> 7) & 1))
        result++;
    }
    if (print)
      std::cerr << std::endl;
  }
  return result;
}

const int space_values [WIDTH * HEIGHT] = {
   40, -30,   5,   4,   4,   5, -30,  40,
  -30, -30,   0,   0,   0,   0, -30, -30,
    5,   0,   1,   1,   1,   1,   0,   5,
    4,   0,   1,   1,   1,   1,   0,   4,
    4,   0,   1,   1,   1,   1,   0,   4,
    5,   0,   1,   1,   1,   1,   0,   5,
  -30, -30,   0,   0,   0,   0, -30, -30,
   40, -30,   5,   4,   4,   5, -30,  40 };

int scoreBoard(Board* board, Side side) {
  int result = 0;
  int final_result = 0;
  int filled = 0;
  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < HEIGHT; j++) {
      if (!board->occupied(i, j)) continue;
      if (board->get(side, i, j)) {
        final_result++;
        result += space_values[i + j * WIDTH];
      } else {
        final_result--;
        result -= space_values[i + j * WIDTH];
      }
      filled++;
    }
  }

  auto moves = board->getMoves(side);
  auto opponent_moves = board->getMoves((Side)!side);
  if (moves.size() == 0 && opponent_moves.size() == 0)
    return final_result > 0 ? 20 : -20;

  if (filled < 32)
    result += 4 * (moves.size() - opponent_moves.size());

  result += 10 * (stable(board, side) - stable(board, (Side)!side));

  return result;
}

int minimax(Board *board, int depth, int alpha, int beta, Side side);
Move *Player::doMinimax(int depth) {
  //std::cerr << stable(&board, side) << std::endl;
  //std::cerr << stable(&board, (Side)!side) << std::endl;
  auto moves = board.getMoves(side);
  if (moves.size() == 0) return NULL;

  int max_value = INT_MIN;
  Move *max_move = NULL;
  int value;
  Board next_board;
  int alpha = INT_MIN;
  
  for (auto move = moves.begin(); move != moves.end(); move++) {
    next_board.fill(&board);
    next_board.doMove(&*move, side);
    value = -minimax(&next_board, depth - 1, alpha, INT_MAX, (Side)!side);
    if (value > max_value) {
      max_value = value;
      if (!max_move) max_move = new Move(0, 0);
      *max_move = *move;
    }
    alpha = max_value > alpha ? max_value : alpha;
  }

  return max_move;
}

int minimax(Board* board, int depth, int alpha, int beta, Side side) {
  if (depth == 0) return scoreBoard(board, side);
  auto moves = board->getMoves(side);
  if (moves.size() == 0) return minimax(board, depth - 1, -alpha, -beta, (Side)!side);

  int max_value = INT_MIN;
  int value;
  Board next_board;

  for (auto move = moves.begin(); move != moves.end(); move++) {
    next_board.fill(board);
    next_board.doMove(&*move, side);
    value = -minimax(&next_board, depth - 1, -beta, -alpha, (Side)!side);
    max_value = value > max_value ? value : max_value;
    alpha = max_value > alpha ? max_value: alpha;
  }
  return max_value;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
  if (opponentsMove) {
    board.doMove(opponentsMove, (Side)!side);
  }

  auto depth = 5;
  if (testingMinimax) depth = 2;
  auto result = doMinimax(depth);

  board.doMove(result, side);

  return result;
}

std::vector<Move> Player::findMoves() {
  return board.getMoves(side);
}

