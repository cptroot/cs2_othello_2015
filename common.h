#ifndef __COMMON_H__
#define __COMMON_H__

#define WIDTH 8
#define HEIGHT 8

#include <sstream>

enum Side { 
    WHITE, BLACK
};

class Move {
   
public:
    int x, y;
    Move(int x, int y) {
        this->x = x;
        this->y = y;        
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }

    std::string to_string() {
      std::stringstream ss;
      ss << this->x;
      ss << ", ";
      ss << this->y;
      return ss.str();
    }
};

#endif
